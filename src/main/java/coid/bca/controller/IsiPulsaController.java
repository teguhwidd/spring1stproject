package coid.bca.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import coid.bca.dao.IsiPulsaDao;
import coid.bca.model.VoucherModel;

@Controller
public class IsiPulsaController {
	@Autowired
	LoginController loginCo;
	@Autowired
	IsiPulsaDao pulsaDao;
	
	@RequestMapping(value = "/isipulsa", method = RequestMethod.GET)
	public String isiPulsa(Model model) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
		model.addAttribute("digit", loginCo.getMathrandom(10, 99));
		return "pages/isi_pulsa";
	}
	@RequestMapping(value = "/doisipulsa", method = RequestMethod.POST)
	public String doIsiulsa(Model model, HttpServletRequest request) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
		model.addAttribute("jenis_voucher", request.getParameter("jenis_voucher"));
		model.addAttribute("nominal", request.getParameter("nominal"));
		model.addAttribute("no_hp", request.getParameter("no_hp"));
		return "pages/confirm_pulsa";
	}
	@RequestMapping(value = "/doKonfirmasiPulsa", method = RequestMethod.POST)
	public String doKonfIsiulsa(@ModelAttribute VoucherModel voucher, Model model, HttpServletRequest request) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
//		HttpSession session = request.getSession();
//		System.out.println(session.getAttribute("username"));
		voucher.setUserid("1");
		voucher.setJenis_voucher(request.getParameter("jenis_voucher"));
		voucher.setNo_hp(request.getParameter("no_hp"));
		voucher.setNominal(request.getParameter("nominal"));
		pulsaDao.insertTrxPulsa(voucher);
		return "pages/notif_pulsa";
	}
}
