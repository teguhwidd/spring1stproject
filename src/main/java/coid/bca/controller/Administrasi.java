package coid.bca.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import coid.bca.dao.AdministrasiDao;
import coid.bca.model.UserModel;

@Controller
public class Administrasi {
	@Autowired
	LoginController loginCo;
	@Autowired
	AdministrasiDao adminDao;
	
	@RequestMapping(value = "/gantibahasa", method = RequestMethod.GET)
	public String ubahBahasa(Model model) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
		return "pages/ubah_bahasa";
	}
	@RequestMapping(value = "/gantipassword", method = RequestMethod.GET)
	public String ubahPassword(Model model) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
		return "pages/ubah_password";
	}
	@RequestMapping(value = "/doubahpassword", method = RequestMethod.POST)
	public String doubahPassword(Model model, UserModel user, HttpServletRequest request) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("username");
		if(adminDao.isValidPassword(username, request.getParameter("passwordlama"))) {
			adminDao.ubahPassword(request.getParameter("upasswordbaru"), username);
			return "pages/notif_password";
		}else {
			model.addAttribute("msg", "Password lama salah");
			return "pages/ubah_password";
		}
	}
}
