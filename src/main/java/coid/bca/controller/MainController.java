package coid.bca.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import coid.bca.dao.LoginDao;

@Controller
public class MainController {
	@Autowired
	LoginController loginCo;
	@RequestMapping(value = { "/", "/welcome**" }, method = RequestMethod.GET)
	public String showFirst(HttpServletRequest request, ModelMap model) {
		model.addAttribute("title", "Spring Security Login Form - Database Authentication");
		model.addAttribute("message", "This is default page!");
		return "pages/login";
	}
	@RequestMapping(value = { "/home" }, method = RequestMethod.GET)
	public String showHome(ModelMap model) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
		model.addAttribute("title", "Spring Security Login Form - Database Authentication");
		model.addAttribute("message", "This is default page!");
		return "pages/home";
	}
}
