package coid.bca.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import coid.bca.dao.PembayaranTeleponDao;
import coid.bca.model.PembTelpModel;
import coid.bca.model.VoucherModel;

@Controller
public class PembayaranTelepon {
	@Autowired
	LoginController loginCo;
	@Autowired
	PembayaranTeleponDao pemTelpDao;
	
	@RequestMapping(value = "/bayartelepon", method = RequestMethod.GET)
	public String bayarTelepon(Model model) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
		return "pages/pembayaran_telepon";
	}
	@RequestMapping(value = "/dobayartelepon", method = RequestMethod.POST)
	public String doBayarTelepon(Model model, HttpServletRequest request) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
		if (request.getParameter("no_hp1") == null) {
			model.addAttribute("nomor",request.getParameter("no_hp2"));
		}else {
			model.addAttribute("nomor",request.getParameter("no_hp1"));
		}
		model.addAttribute("provider", request.getParameter("provider"));
		model.addAttribute("nominal", request.getParameter("nominal"));
		return "pages/confirm_bayartelp";
	}
	@RequestMapping(value = "/comfbayartelepon", method = RequestMethod.POST)
	public String confirmBayarTelepon(Model model, HttpServletRequest request) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
		if (request.getParameter("no_hp1") == null) {
			model.addAttribute("nomor",request.getParameter("no_hp2"));
		}else {
			model.addAttribute("nomor",request.getParameter("no_hp1"));
		}
		model.addAttribute("provider", request.getParameter("provider"));
		model.addAttribute("nominal", request.getParameter("nominal"));
		return "pages/confirm_bayartelp";
	}
	@RequestMapping(value = "/doKonfBayarTelp", method = RequestMethod.POST)
	public String doKonfBayarTelp(PembTelpModel pembTelp, Model model, HttpServletRequest request) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
		HttpSession session = request.getSession();
		pembTelp.setUserid((String)session.getAttribute("userid"));
		System.out.println("USER ID ="+(String)session.getAttribute("userid"));
		pembTelp.setProvider(request.getParameter("provider"));
		pembTelp.setNomor(request.getParameter("nomor"));
		pembTelp.setNominal(request.getParameter("nominal"));
		pemTelpDao.insertBayarTelp(pembTelp);
		return "pages/notif_bayartelp";
	}
}
