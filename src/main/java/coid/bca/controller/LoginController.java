package coid.bca.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import coid.bca.dao.LoginDao;
import coid.bca.model.UserModel;

@Controller
public class LoginController {
	@Autowired
	LoginDao loginDao;
	
	
	@RequestMapping(value = "/doLogin", method = RequestMethod.POST)
	public String doLogin2(UserModel userModel, ModelMap model, HttpServletRequest request) {
		if(loginDao.isValidUser2(userModel)) {
			userModel.setUsername(request.getParameter("username"));
			userModel.setPassword(request.getParameter("password"));
//			List<UserModel> usr = loginDao.getUser();
			model.addAttribute("msg", "Login Success");
//			model.addAttribute("Usr", usr);
			model.addAttribute("tanggal", getTanggal());
			model.addAttribute("jam", getJam());
			HttpSession session = request.getSession();
			session.setAttribute("password", request.getParameter("password"));
			session.setAttribute("username", request.getParameter("username"));
			session.setAttribute("userid", "1");
			return "pages/home";
		}else {
			model.addAttribute("msg", "Invalid Username and Password");
			return "pages/login";
		}
	}
	
	@RequestMapping(value = "/doLogin2", method = RequestMethod.POST)
	public String doLogin(UserModel userModel, ModelMap model, HttpServletRequest request) {
		if(loginDao.isValidUser(request.getParameter("username"), request.getParameter("password"))) {
			model.addAttribute("msg", "Login Success");
			HttpSession session = request.getSession();
			session.setAttribute("username", request.getParameter("username"));
			session.setAttribute("userid", "1");
			return "pages/home";
		}else {
			model.addAttribute("msg", "Invalid Username and Password");
			return "pages/login";
		}
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String showForm(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.invalidate();
		return "login/login";
	}

	@RequestMapping(value = "/tampil", method = RequestMethod.GET)
	public String showForm(ModelMap model) {
		List<UserModel> usr = loginDao.getAll();
		model.addAttribute("Usr", usr);
		return "tampil";
	}
	
	@RequestMapping(value = "/admin**", method = RequestMethod.GET)
	public String adminPage(Model model) {
		model.addAttribute("title", "Spring Security Login Form - Database Authentication");
		model.addAttribute("message", "This page is for ROLE_ADMIN only!");
		return "admin";

	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(Model model, @RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout) {

	  if (error != null) {
		  model.addAttribute("error", "Invalid username and password!");
	  }
	  if (logout != null) {
		  model.addAttribute("msg", "You've been logged out successfully.");
	  }
	  return "pages/login";

	}
	
	//for 403 access denied page
		@RequestMapping(value = "/403", method = RequestMethod.GET)
		public String accesssDenied(Model model) {
		  //check if user is login
		  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		  if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();	
			model.addAttribute("username", userDetail.getUsername());
		  }
		  return "403";
		}

	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public String testing(Model model) {
		model.addAttribute("total", Integer.toString(loginDao.countUser()));
		return "count";
	}

	@RequestMapping(value = "/login1", method = RequestMethod.GET)
	public String showLogin(HttpServletRequest request, ModelMap model) {
		HttpSession session = request.getSession(false);
		if(("teguh").equals(session.getAttribute("username"))) {
			return "home";
		} else {
			model.addAttribute("msg", "Please Enter Your Login Details");
			return "login/login";
		}
	}
	
	public String getTanggal() {
		String now= "";
		Date dNow = new Date( );
	    SimpleDateFormat ft = new SimpleDateFormat ("dd/MM/yyyy");
	    return now =" "+ft.format(dNow);
	}
	public String getJam() {
		String now= "";
		Date dNow = new Date( );
		SimpleDateFormat ft = new SimpleDateFormat ("hh:mm:ss");
		return now =" "+ft.format(dNow);
	}
	public int getMathrandom(int min, int max) {
	   int range = (max - min) + 1;     
	   return (int)(Math.random() * range) + min;
	}
}
