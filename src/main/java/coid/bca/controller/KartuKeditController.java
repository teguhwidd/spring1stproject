package coid.bca.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import coid.bca.dao.KartuKreditDao;
import coid.bca.model.CCTagihanModel;
import coid.bca.model.CCTransaksiModel;
import coid.bca.model.UserModel;

@Controller
public class KartuKeditController {
	@Autowired
	LoginController loginCo;
	@Autowired
	KartuKreditDao kartuDao;
	
	@RequestMapping(value = "/transaksicc", method = RequestMethod.GET)
	public String getTrxCC(Model model) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
		List<CCTransaksiModel> kartu = kartuDao.getTrxThisMonth();
		model.addAttribute("Kartu", kartu);
		return "pages/transaksi_kartu";
	}
	@RequestMapping(value = "/tagihancc", method = RequestMethod.GET)
	public String getTagihanCC(Model model) {
		model.addAttribute("tanggal", loginCo.getTanggal());
		model.addAttribute("jam", loginCo.getJam());
		List<CCTransaksiModel> kartu = kartuDao.getTrxPrevMonth();
		List<CCTagihanModel> tagihan = kartuDao.getTagihanPrevMonth();
		model.addAttribute("Kartu", kartu);
		model.addAttribute("Tagihan", tagihan);
		return "pages/tagihan_kartu";
	}
}
