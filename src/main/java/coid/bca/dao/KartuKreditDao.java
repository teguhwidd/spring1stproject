package coid.bca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import coid.bca.model.CCTagihanModel;
import coid.bca.model.CCTransaksiModel;
import coid.bca.model.UserModel;

@Service
public class KartuKreditDao {
	@Autowired
	JdbcTemplate template;
	public List<CCTransaksiModel> getAllTransaksi() {
		String query = "SELECT * FROM transaksi_cc";
		List<CCTransaksiModel> ccModel = template.query(query, new BeanPropertyRowMapper(CCTransaksiModel.class));
		return ccModel;
	}
	public List<CCTransaksiModel> getTrxThisMonth() {
		String query = "SELECT * FROM transaksi_cc WHERE tgl_transaksi "
				+ "BETWEEN TO_DATE('01-09-2018', 'dd-MM-yyyy') "
				+ "AND TO_DATE('30-09-2018', 'dd-MM-yyyy')";
		List<CCTransaksiModel> ccModel = template.query(query, new BeanPropertyRowMapper(CCTransaksiModel.class));
		return ccModel;
	}
	public List<CCTransaksiModel> getTrxPrevMonth() {
		String query = "SELECT * FROM transaksi_cc WHERE tgl_transaksi "
				+ "BETWEEN TO_DATE('01-08-2018', 'dd-MM-yyyy') "
				+ "AND TO_DATE('30-08-2018', 'dd-MM-yyyy')";
		List<CCTransaksiModel> ccModel = template.query(query, new BeanPropertyRowMapper(CCTransaksiModel.class));
		return ccModel;
	}
	public List<CCTagihanModel> getTagihanPrevMonth() {
		String query = "SELECT * FROM tagihan_cc WHERE tgl_transaksi "
				+ "BETWEEN TO_DATE('01-08-2018', 'dd-MM-yyyy') "
				+ "AND TO_DATE('30-08-2018', 'dd-MM-yyyy')";
		List<CCTagihanModel> ccTgModel = template.query(query, new BeanPropertyRowMapper(CCTagihanModel.class));
		return ccTgModel;
	}
}
