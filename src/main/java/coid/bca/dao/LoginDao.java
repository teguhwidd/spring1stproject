package coid.bca.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import coid.bca.model.LoginModel;
import coid.bca.model.UserModel;

@Service
public class LoginDao {
	@Autowired
	JdbcTemplate template;

	public int countUser() {
//		template.query(sql, pss, rse)
		String query = "SELECT COUNT(*) FROM users";
		int result = template.queryForObject(query, Integer.class);
		return result;

	}
	public boolean isValidUser(String username, String password) {
//		System.out.println("==================="+username+password);
		String query = "SELECT * FROM users WHERE username= ? AND password= ?";
		int count = template.update(query, username, password);
//		System.out.println("####################### count :" +count);
		if (count > 0) {
			return true;
		}else {
			return false;
		}
	}
	public boolean isValidUser2(UserModel user) {
		String query = "SELECT * FROM users WHERE username= ? AND password= ?";
		int count = template.update(query, user.getUsername(), user.getPassword());
//		System.out.println("####################### count :" +count);
		if (count > 0) {
			return true;
		}else {
			return false;
		}
	}

	public List<UserModel> getAll() {
		String query = "SELECT * FROM users";
		List<UserModel> userModel = template.query(query, new BeanPropertyRowMapper(UserModel.class));
		return userModel;
	}
	public List<UserModel> getUser(UserModel user) {
		String query = "SELECT * FROM users WHERE username= "+user.getUsername()+"";
		List<UserModel> userModel = template.query(query, new BeanPropertyRowMapper(UserModel.class));
		return userModel;
	}

}
