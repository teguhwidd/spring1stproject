package coid.bca.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import coid.bca.model.UserModel;

@Service
public class AdministrasiDao {
	@Autowired
	JdbcTemplate template;
	
	public boolean isValidPassword(String username, String password) {
		String query = "SELECT password FROM users WHERE username= ? AND password= ?";
		int count = template.update(query, username, password);
//		System.out.println("####################### count :" +count);
		if (count > 0) {
			return true;
		}else {
			return false;
		}
	}
	public void ubahPassword(String newpasssword, String username) {
		String query = "UPDATE users SET password = ? WHERE username = ?";
		template.update(query, newpasssword, username);
	}
}
