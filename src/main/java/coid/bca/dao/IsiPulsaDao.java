package coid.bca.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import coid.bca.model.UserModel;
import coid.bca.model.VoucherModel;

@Service
public class IsiPulsaDao {
	@Autowired
	JdbcTemplate template;
	
	public void insertTrxPulsa(VoucherModel voucher) {
		String query = "INSERT INTO VOUCHER (userid,no_hp,jenis_voucher,nominal) VALUES (?,?,?,?)";
		template.update(query, voucher.getUserid(), 
				voucher.getNo_hp(), voucher.getJenis_voucher(),voucher.getNominal());
	}
}
