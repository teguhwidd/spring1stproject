package coid.bca.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import coid.bca.model.PembTelpModel;
import coid.bca.model.VoucherModel;

@Service
public class PembayaranTeleponDao {
	@Autowired
	JdbcTemplate template;
	
	public void insertBayarTelp(PembTelpModel pmbTelp) {
		String query = "INSERT INTO bayar_telp (userid,nomor,provider,nominal) VALUES (?,?,?,?)";
		template.update(query, pmbTelp.getUserid(), 
				pmbTelp.getNomor(), pmbTelp.getProvider(),pmbTelp.getNominal());
	}
}
