package coid.bca.model;

public class VoucherModel {
	private String voucherid;
	private String userid;
	private String no_hp;
	private String jenis_voucher;
	private String nominal;
	
	public String getVoucherid() {
		return voucherid;
	}
	public void setVoucherid(String voucherid) {
		this.voucherid = voucherid;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getNo_hp() {
		return no_hp;
	}
	public void setNo_hp(String no_hp) {
		this.no_hp = no_hp;
	}
	public String getJenis_voucher() {
		return jenis_voucher;
	}
	public void setJenis_voucher(String jenis_voucher) {
		this.jenis_voucher = jenis_voucher;
	}
	public String getNominal() {
		return nominal;
	}
	public void setNominal(String nominal) {
		this.nominal = nominal;
	}
	
	
}
