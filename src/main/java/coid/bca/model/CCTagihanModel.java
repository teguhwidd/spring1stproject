package coid.bca.model;

import java.util.Date;

public class CCTagihanModel {
	private String idtransaksi;
	private Date tglTransaksi;
	private Date tglPosting;
	private String keterangan;
	private Integer nominal;
	public String getIdtransaksi() {
		return idtransaksi;
	}
	public void setIdtransaksi(String idtransaksi) {
		this.idtransaksi = idtransaksi;
	}
	
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public Integer getNominal() {
		return nominal;
	}
	public void setNominal(Integer nominal) {
		this.nominal = nominal;
	}
	public Date getTglTransaksi() {
		return tglTransaksi;
	}
	public void setTglTransaksi(Date tglTransaksi) {
		this.tglTransaksi = tglTransaksi;
	}
	public Date getTglPosting() {
		return tglPosting;
	}
	public void setTglPosting(Date tglPosting) {
		this.tglPosting = tglPosting;
	}
	
	
}
