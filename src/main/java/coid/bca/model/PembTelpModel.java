package coid.bca.model;

public class PembTelpModel {
	private String idpembayaran;
	private String userid;
	private String nomor;
	private String provider;
	private String nominal;
	public String getIdpembayaran() {
		return idpembayaran;
	}
	public void setIdpembayaran(String idpembayaran) {
		this.idpembayaran = idpembayaran;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getNomor() {
		return nomor;
	}
	public void setNomor(String nomor) {
		this.nomor = nomor;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getNominal() {
		return nominal;
	}
	public void setNominal(String nominal) {
		this.nominal = nominal;
	}
	
	
}
