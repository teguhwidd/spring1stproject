<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link href="<c:url value="/resources/css/bootstrap-3.3.7/dist/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/js/jquery-3.1.1.js" />"></script>

<meta charset="ISO-8859-1">
<title>Klick BCA</title>
</head>
<body>
	Ini halaman Utama
	<h1>1. Test CSS</h1>

	<h2>2. Test JS</h2>
	<div id="msg"></div>
	<img src="<c:url value="/resources/assets/bca_logo.png" />" alt="BCA" />
	<script>
		jQuery(document).ready(function($) {

			$('#msg').html("This is updated by jQuery")

		});
	</script>

</body>
</html>