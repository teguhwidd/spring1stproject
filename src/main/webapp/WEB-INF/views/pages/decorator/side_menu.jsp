<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="sidenav">
	<button class="dropdown-btn">
		<spring:message code="label.pembelian" /> <i class="fa fa-caret-down"></i>
	</button>
	<div class="dropdown-container">
		<a href="<c:url value="/isipulsa" />"><spring:message code="label.isipulsa" /></a>
	</div>
	<button class="dropdown-btn">
		<spring:message code="label.pembayaran" /> <i class="fa fa-caret-down"></i>
	</button>
	<div class="dropdown-container">
		<a href="<c:url value="bayartelepon" />"><spring:message code="label.telepon" /></a>
	</div>
	<button class="dropdown-btn">
		<spring:message code="label.kartukredit" /> <i class="fa fa-caret-down"></i>
	</button>
	<div class="dropdown-container">
		<a href="<c:url value="transaksicc" />"><spring:message code="label.transaksi" /></a>
		<a href="<c:url value="tagihancc" />"><spring:message code="label.tagihan" /></a>
	</div>
	<button class="dropdown-btn">
		<spring:message code="label.administrasi" /> <i class="fa fa-caret-down"></i>
	</button>
	<div class="dropdown-container">
		<a href="<c:url value="gantipassword" />"><spring:message code="label.gantipass" /></a>
		<a href="<c:url value="gantibahasa" />"><spring:message code="label.gantibahasa" /></a>
	</div>
</div>