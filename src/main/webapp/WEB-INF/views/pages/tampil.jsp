<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Home</title>
</head>
<body>
	<div class="container">
		<div class="box">
			<div class="header">
				<div class="up"></div>
				<div class="down">
					<div class="left">INDIVIDUAL</div>
					<div class="right">Home</div>
				</div>
				<div class="klik">
					<img src="<c:url value="/resources/assets/klick_bca.png" />"
						alt="klikBCA" height="80" width="80" />
				</div>

			</div>
			<div class="body">
				<div class="side">
					<div class="sidenav">
						<button class="dropdown-btn">
							Pembelian <i class="fa fa-caret-down"></i>
						</button>
						<div class="dropdown-container">
							<a href="#">Pulsa isi ulang</a> 
						</div>
						<button class="dropdown-btn">
							Pembayaran <i class="fa fa-caret-down"></i>
						</button>
						<div class="dropdown-container">
							<a href="#">Telepon</a> 
						</div>
						<button class="dropdown-btn">
							Kartu Kredit <i class="fa fa-caret-down"></i>
						</button>
						<div class="dropdown-container">
							<a href="#">Transaksi</a> 
							<a href="#">Tagihan</a> 
						</div>
						<button class="dropdown-btn">
							Administrasi<i class="fa fa-caret-down"></i>
						</button>
						<div class="dropdown-container">
							<a href="#">Ganti Bahasa</a> 
							<a href="#">Ganti Password</a> 
						</div>
					</div>
				</div>
				<div class="main"></div>
				<div class="footer"> Copy right &copy; 2018 <img src="<c:url value="/resources/assets/bca_logo.png" />" alt="" height="15px"/></div>
			</div>
			
		</div>
	</div>
	
	<h1>Ini login 2</h1>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				Ini login 2 ${msg }
				<form action="/Spring1stProject/doLogin" method="post">
					Username <input type="text" name="username" /> <br /> Password <input
						type="password" name="password" /> <br /> <input type="submit"
						value="Login" />
				</form>
			</div>
		</div>
	</div>
	<!-- <img src="<c:url value="/resources/assets/klick_bca.png" />" alt="BCA" /> -->
	<script>
		var dropdown = document.getElementsByClassName("dropdown-btn");
		var i;

		for (i = 0; i < dropdown.length; i++) {
			dropdown[i].addEventListener("click", function() {
				this.classList.toggle("active");
				var dropdownContent = this.nextElementSibling;
				if (dropdownContent.style.display === "block") {
					dropdownContent.style.display = "none";
				} else {
					dropdownContent.style.display = "block";
				}
			});
		}
	</script>
</body>
</html>