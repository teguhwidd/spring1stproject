<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="decorator/page_header.jsp"></jsp:include>
<meta charset="ISO-8859-1">
<title>BCA</title>
</head>
<body>
	<div class="container">
		<div class="box">
			<div class="header">
				<jsp:include page="decorator/header.jsp"></jsp:include>
			</div>
			<div class="body">
				<div class="side">
				<jsp:include page="decorator/side_menu.jsp"></jsp:include>
				</div>
				<div class="main">
					<div class="time">
					<spring:message code="label.tanggal" />${tanggal} <spring:message code="label.jam" />${jam}
					</div>
					<div class="left"></div>
					<div class="judul"><spring:message code="label.pembayaran_telepon" /></div>
					<div class="jeda"></div>
					<div class="content">
						<table>
						<form action="dobayartelepon" method="post">
							<tr>
								<td colspan="2">SILAHKAN PILIH JENIS PENGISIAN</td>
							</tr>
							<tr>
								<td style="width: 300px;"><input type="radio" name="opsi" value="A" checked="checked"/><span>MASUKAN NOMER TELEPON</span></td>
								<td>: <input type="number" name="no_hp1" id="no_hp1" /></td>
							</tr>
							<tr>
								<td style="width: 300px;"><input type="radio" name="opsi" value="B"/>DARI DAFTAR PEMBAYARAN</td>
								<td>: 
									<select name="no_hp2" id="no_hp2" disabled="disabled">
									  <option value=""></option>
									  <option value="085600029000">085600029000</option>
									  <option value="085600060026">085600060026</option>
									  <option value="085600060036">085600060036</option>
									</select>
								</td>
							</tr>
							<tr>
								<td style="">OPERATOR TELEPON</td>
								<td style="">: 
									<select name="provider">
									  <option value="telkomsel">TELKOMSEL</option>
									  <option value="xl">XL</option>
									  <option value="indosat">INDOSAT</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>NOMINAL </td>
								<td>: 
								<input type="number" name="nominal" />
								</td>
							</tr>
							
						</table> <br />
						<input style="float:right" type="submit" value="Lanjutkan"/>
						</form>
					</div>
				</div>
				<div class="footer"> Copy right &copy; 2018 <img src="<c:url value="/resources/assets/bca_logo.png" />" alt="" height="15px"/></div>
			</div>
			
		</div>
	</div>
	
	<!-- <img src="<c:url value="/resources/assets/klick_bca.png" />" alt="BCA" /> -->
	<jsp:include page="decorator/page_footer.jsp"></jsp:include>
	<script>

	$('input:radio[name="opsi"]').change(function() {
		if ($(this).val() == 'A') {
			$("#no_hp1").prop('disabled', false);
			$("#no_hp2").prop('disabled', true);
			$("#no_hp2").val("");
		} else {
			$("#no_hp1").prop('disabled', true);
			$("#no_hp1").val("");
			$("#no_hp2").prop('disabled', false);
		}
	});
	</script>
</body>
</html>