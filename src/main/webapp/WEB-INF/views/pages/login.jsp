<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="decorator/page_header.jsp"></jsp:include>
<meta charset="ISO-8859-1">
<title>Home</title>
</head>
<body>
	<div class="container">
		<div class="box">
			<div class="header">
				<jsp:include page="decorator/header.jsp"></jsp:include>
			</div>
			<div class="body">
				<div class="side">
					<div class="caption-white">
					User ID dan PIN internet Banking dapat diperoleh pada saat 
					Anda melakukan Registrasi Internet melalui ATM BCA. 
					Untuk informasi lebih lanjut hubungi Halo BCA (021)52999888
					</div>
				</div>
				<div class="main">
					<div class="content">
						<div class="isi">
						<form action="/Spring1stProject/doLogin" method="post">
							<span class="underline">Silahkan memasukan USER ID Anda</span> <br />
							<span class="orange">Please Enter your USER ID</span> <br />
							<input type="text" name="username" /> <br /> <br />
							<span class="underline">Silahkan memasukan PIN Internet banking Anda</span> <br />
							<span class="orange">Please Enter your Internet Banking Pin</span> <br />
							<input type="password" name="password" /> <br /> <br />
							<input type="submit" value="Login" /> <br />
						</form>
						</div>
						
					</div>
				</div>
				<jsp:include page="decorator/footer.jsp"></jsp:include>
			</div>
		</div>
	</div>

	<!-- <img src="<c:url value="/resources/assets/klick_bca.png" />" alt="BCA" /> -->
	<jsp:include page="decorator/page_footer.jsp"></jsp:include>
</body>
</html>