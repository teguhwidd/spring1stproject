<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="decorator/page_header.jsp"></jsp:include>
<meta charset="ISO-8859-1">
<title>BCA</title>
</head>
<body>
	<div class="container">
		<div class="box">
			<div class="header">
				<jsp:include page="decorator/header.jsp"></jsp:include>
			</div>
			<div class="body">
				<div class="side">
				<jsp:include page="decorator/side_menu.jsp"></jsp:include>
				</div>
				<div class="main">
					<div class="time">
					<spring:message code="label.tanggal" />${tanggal} <spring:message code="label.jam" />${jam}
					</div>
					<div class="left"></div>
					<div class="judul"><spring:message code="label.pembelian_pulsa" /></div>
					<div class="jeda" style="color: red">Konfirmasi Pembayaran telepon</div>
					<div class="content">
						<table>
						<form action="doKonfBayarTelp" method="post">
							<tr>
								<td style="">NOMER</td>
								<td style="">: <span>${nomor }</span>
									<input type="hidden" name="nomor" value="${nomor }" />
								</td>
							</tr>
							<tr>
								<td>PROVIDER</td>
								<td>: <span>${provider }</span>
									<input type="hidden" name="provider" value="${provider}" /> 
								</td>
							</tr>
							<tr>
								<td>NOMINAL </td>
								<td>: <span>${nominal}</span>
								<input type="hidden" name="nominal" value="${nominal}"/>
								</td>
							</tr>
							<tr>
								<td>Respon KeyBCA Appli2</td>
								<td>: <input type="number" size="6"/></td>
							</tr>
						</table> <br />
						<input style="float:right" type="submit" value="Bayar"/>&ensp;<a href="bayartelepon">Kembali</a>
						</form>
					</div>
				</div>
				<div class="footer"> Copy right &copy; 2018 <img src="<c:url value="/resources/assets/bca_logo.png" />" alt="" height="15px"/></div>
			</div>
			
		</div>
	</div>
	
	<!-- <img src="<c:url value="/resources/assets/klick_bca.png" />" alt="BCA" /> -->
	<jsp:include page="decorator/page_footer.jsp"></jsp:include>
</body>
</html>