<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="decorator/page_header.jsp"></jsp:include>
<meta charset="ISO-8859-1">
<title>BCA</title>
</head>
<body>
	<div class="container">
		<div class="box">
			<div class="header">
				<jsp:include page="decorator/header.jsp"></jsp:include>
			</div>
			<div class="body">
				<div class="side">
				<jsp:include page="decorator/side_menu.jsp"></jsp:include>
				</div>
				<div class="main">
					<div class="time">
					<spring:message code="label.tanggal" />${tanggal} <spring:message code="label.jam" />${jam}
					</div>
					<div class="left"></div>
					<div class="judul"><spring:message code="label.pembelian_pulsa" /></div>
					<div class="jeda"></div>
					<div class="content">
						<table>
						<form action="doisipulsa" method="post">
							<tr>
								<td style="">JENIS VOUCHER</td>
								<td style="">: 
									<select name="jenis_voucher">
									  <option value="telkomsel">TELKOMSEL</option>
									  <option value="xl">XL</option>
									  <option value="indosat">INDOSAT</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>NOMINAL VOUCHER (Rp.)</td>
								<td>: 
									<select name="nominal">
									  <option value="20000">Rp. 20,000</option>
									  <option value="50000">Rp. 50,000</option>
									  <option value="100000">Rp. 100,000</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>NOMER HANDPHONE </td>
								<td>: 
								<input type="number" name="no_hp" id="no_hp" />
								</td>
							</tr>
							<tr>
								<td style="color: red">* Pastikan 6 Angka terakhir sesuai nomer telepon</td>
								<td>: 
									<input type="text" value="${digit }" style="width: 20px;" disabled="disabled"/>
									<input type="number" id="token" style="width: 60px;" disabled="disabled"/>
								</td>
							</tr>
							<tr>
								<td>Respon KeyBCA Appli2</td>
								<td>: <input type="number" size="6"/></td>
							</tr>
						</table> <br />
						<input style="float:right" type="submit" value="Lanjutkan"/>
						</form>
					</div>
				</div>
				<div class="footer"> Copy right &copy; 2018 <img src="<c:url value="/resources/assets/bca_logo.png" />" alt="" height="15px"/></div>
			</div>
			
		</div>
	</div>
	
	<!-- <img src="<c:url value="/resources/assets/klick_bca.png" />" alt="BCA" /> -->
	<jsp:include page="decorator/page_footer.jsp"></jsp:include>
	<script>
	$(document).ready(function(){
	    $("#no_hp").focusout(function(){
	        var id = $("#no_hp").val();
	        var lastsix = id.substr(id.length - 6);
	        $("#token").val(lastsix);
	    });
	});
	</script>
</body>
</html>