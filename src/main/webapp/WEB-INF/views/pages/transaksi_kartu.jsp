<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="decorator/page_header.jsp"></jsp:include>
<meta charset="ISO-8859-1">
<title>BCA</title>
</head>
<body>
	<div class="container">
		<div class="box">
			<div class="header">
				<jsp:include page="decorator/header.jsp"></jsp:include>
			</div>
			<div class="body">
				<div class="side">
				<jsp:include page="decorator/side_menu.jsp"></jsp:include>
				</div>
				<div class="main">
					<div class="time">
					<spring:message code="label.tanggal" />${tanggal} <spring:message code="label.jam" />${jam}
					</div>
					<div class="left"></div>
					<div class="judul">KARTU KREDIT - TRANSAKSI</div>
					<div class="jeda blue">DAFTAR TRANSAKSI BULAN INI</div>
					<div class="content">
						<span style="color: blue; font-weight: bold;">Nomor Kartu &ensp;&ensp;&ensp;: &ensp;</span>
						<span style="color: blue; font-weight: bold;">44364234234234</span>
						<table>
							<tr>
								<th>#</th>
								<th>Tgl Transaksi</th>
								<th>Tgl Posting</th>
								<th>Keterangan</th>
								<th>Nominal</th>
							</tr>
							<c:forEach items="${Kartu}" var="kartu" varStatus="counter">
							<tr>
								<!-- <td><c:out value="${fn:length(Kartu)}" /></td> -->
								<td>${counter.count}</td>
								<td><fmt:formatDate pattern="dd-MMM-yyyy" value="${kartu.tglPosting}" /></td>
								<td><fmt:formatDate pattern="dd-MMM-yyyy" value="${kartu.tglTransaksi}" /></td>
			            		<td><c:out value="${kartu.keterangan}" /></td>
								<td><c:out value="${kartu.nominal}" /></td>
							</tr>
							</c:forEach>
						</table> <br />
						<!-- <input style="float:right" type="submit" value="Lanjutkan"/> -->
					</div>
				</div>
				<div class="footer"> Copy right &copy; 2018 <img src="<c:url value="/resources/assets/bca_logo.png" />" alt="" height="15px"/></div>
			</div>
			
		</div>
	</div>
	
	<!-- <img src="<c:url value="/resources/assets/klick_bca.png" />" alt="BCA" /> -->
	<jsp:include page="decorator/page_footer.jsp"></jsp:include>
	<script>
	</script>
</body>
</html>