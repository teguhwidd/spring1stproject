<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="decorator/page_header.jsp"></jsp:include>
<meta charset="ISO-8859-1">
<title>Home</title>
</head>
<body>
	<div class="container">
		<div class="box">
			<div class="header">
				<jsp:include page="decorator/header.jsp"></jsp:include>
			</div>
			<div class="body">
				<div class="side">
					<jsp:include page="decorator/side_menu.jsp"></jsp:include>
				</div>
				<div class="main">
					<div class="time">
					<spring:message code="label.tanggal" />${tanggal} <spring:message code="label.jam" />${jam}
					</div>
					<div class="left"></div>
					<div class="judul">ADMINISTRASI - UBAH BAHASA</div>
					<div class="last"></div>
					<div class="content">
						<div class="isi">
							<center>
								<a class="eng" href="<c:url value="?lang=en" /> ">
								English
								</a> <br />
        						<a class="ind" href="<c:url value="?lang=ind" />">
        						Indonesia 
								</a>
							</center>
						</div>
						
					</div>
				</div>
				<jsp:include page="decorator/footer.jsp"></jsp:include>
			</div>
		</div>
	</div>

	<!-- <img src="<c:url value="/resources/assets/klick_bca.png" />" alt="BCA" /> -->
	<jsp:include page="decorator/page_footer.jsp"></jsp:include>
	<script>
	$('a.eng').click(function(e){
		if (confirm("Yakin ingin merubah bahasa ?")) {
	    } else {
	    	e.preventDefault();
	    }
	});
	$('a.ind').click(function(e){
		if (confirm("Yakin ingin merubah bahasa ?")) {
	    } else {
	    	e.preventDefault();
	    }
	});
	</script>
</body>
</html>