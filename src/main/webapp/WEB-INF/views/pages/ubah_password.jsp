<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="decorator/page_header.jsp"></jsp:include>
<meta charset="ISO-8859-1">
<title>BCA</title>
</head>
<body>
	<div class="container">
		<div class="box">
			<div class="header">
				<jsp:include page="decorator/header.jsp"></jsp:include>
			</div>
			<div class="body">
				<div class="side">
				<jsp:include page="decorator/side_menu.jsp"></jsp:include>
				</div>
				<div class="main">
					<div class="time">
					<spring:message code="label.tanggal" />${tanggal} <spring:message code="label.jam" />${jam}
					</div>
					<div class="left"></div>
					<div class="judul">ADMINISTRASI - UBAH PASSWORD</div>
					<div class="jeda"></div>
					<div class="content">
						<table>
						<form action="doubahpassword" method="post">
							<tr>
								<td style="">PASSWORD LAMA</td>
								<td style="">:
								<input type="password" name="passwordlama" /> 
								</td>
							</tr>
							<tr>
								<td>PASSWORD BARU</td>
								<td>: 
								<input type="password" name="passwordbaru" />
								</td>
							</tr>
							<tr>
								<td>ULANGI PASSWORD BARU</td>
								<td>: 
								<input type="password" name="upasswordbaru" />
								</td>
							</tr>
						</table> <br />
						<input style="float:left" type="submit" value="UBAH"/>
						</form>
					</div>
				</div>
				<div class="footer"> Copy right &copy; 2018 <img src="<c:url value="/resources/assets/bca_logo.png" />" alt="" height="15px"/></div>
			</div>
			
		</div>
	</div>
	
	<!-- <img src="<c:url value="/resources/assets/klick_bca.png" />" alt="BCA" /> -->
	<jsp:include page="decorator/page_footer.jsp"></jsp:include>
</body>
</html>