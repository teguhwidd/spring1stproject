<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Insert title here</title>
	</head>
	<body>
		<h1><a href="/InitMavenProjectDemo/addUser">ADD USER</a></h1>
		<table border="1">
				<tr>
					<th>USER ID</th>
					<th>NAME</th>
					<th>PASSWORD</th>
					<th colspan="2">ACTION</th>
				</tr>
			    <c:forEach items="${Usr}" var="user">
			        <tr>
			            <td>${user.userid}</td>
			            <td><c:out value="${user.name}" /></td>
			            <td><c:out value="${user.password}" /></td>
			            <td><a href="/InitMavenProjectDemo/editUser?userid=${user.userid}&name=${user.name}&password=${user.password}">EDIT</a></td>
			            <td><a href="/InitMavenProjectDemo/delUser?userid=${user.userid}">DELETE</a></td>
			        </tr>
			    </c:forEach>
		</table>

	</body>
</html>